import java.util.Scanner;
public class Hangman{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("give a word");
		String word = reader.nextLine();
		System.out.print("\033[h\033[2J");
		System.out.flush();
		runGame(word);
	}
	public static int isLetterInWord(String word, char c){
		
	for(int i = 0 ; i < word.length() ; i++){
		if (toUpperCase(word.charAt(i)) == toUpperCase(c)){
			return i;
		}
	}	
	return -1;
	}
	public static char toUpperCase (char c){
		return Character.toUpperCase(c);
	}
	public static void printWork (String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		boolean[] wordArray = {letter0, letter1, letter2, letter3};
		char[] shownWord = new char[]{'_','_','_','_'} ;
		for (int i = 0 ; i < wordArray.length ; i++){
			if (wordArray[i] == true){
				shownWord[i] = word.charAt(i);
			}
		}
		for (int i = 0 ; i < wordArray.length ; i++){
			System.out.print(shownWord[i]);
		}
		
	}
	public static void runGame(String word){
		Scanner reader = new Scanner(System.in);
		boolean[] wordArray = new boolean [4];
		int lives = 6;
		while ((wordArray[0] == false || wordArray[1] == false || wordArray[2] == false || wordArray[3] == false)&&(lives > 0) ){
			System.out.println("\r\n" +"Guess a letter");
			char guess = reader.nextLine().charAt(0);
			if ( isLetterInWord(word, guess) !=  -1){
			wordArray[isLetterInWord(word, guess)] = true;
			}
			else{
				lives--;
				System.out.println("WRONG");
				System.out.println("lives left = " + lives);
				
			}
			printWork(word, wordArray[0], wordArray[1], wordArray[2], wordArray[3]);
		}
		if (lives > 0){
			System.out.println("\r\n YOU WON!");
		}
		else{
			System.out.println("\r\n YOU LOST, LOSER!!!!!");
		}
	}
		
}